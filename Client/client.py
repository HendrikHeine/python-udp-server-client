#Beispielskript für Raspberry Pi Zero W

import socket
from gpiozero import LED
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serverIP = "0.0.0.0"
serverPORT = 10000

pin1 = 17
pin2 = 27
outOK = "OK"
outFAIL = "FAIL"

id = "A001"
RECV_BUFFER = ""
server = (serverIP, serverPORT)
sock.bind(server)

myRelai_1 = "OFF"
myRelai_2 = "OFF"

while True:
    payload, client_address = sock.recvfrom(1024)
    RECV_BUFFER = str(payload.decode())
    data = RECV_BUFFER.split(";")
    clientIP = str(client_address).split("'")
    clientPORT = clientIP[2].split(" ")
    clientPORT = clientPORT[1].split(")")
    clientPORT = clientPORT[0]
    clientIP = clientIP[1]
    if len(data) == 3 and data[0] == id:

        if data[1] == "LED_01":
            if data[2] == "ON":
                if myRelai_1 == "OFF":
                    myRelai_1 = LED(pin1)
                    myRelai_1.on()
                    sock.sendto(outOK.encode(), (str(clientIP), int(clientPORT)))

            elif data[2] == "OFF":
                myRelai_1 = "OFF"
                sock.sendto(outOK.encode(), (str(clientIP), int(clientPORT)))

        elif data[1] == "LED_02":
            if data[2] == "ON":
                if myRelai_2 == "OFF":
                    myRelai_2 = LED(pin2)
                    myRelai_2.on()
                    sock.sendto(outOK.encode(), (str(clientIP), int(clientPORT)))

            elif data[2] == "OFF":
                myRelai_2 = "OFF"
                sock.sendto(outOK.encode(), (str(clientIP), int(clientPORT)))

    else:
        sock.sendto(outFAIL.encode(), (str(clientIP), int(clientPORT)))
        break


