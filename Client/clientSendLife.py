import socket
import datetime
from datetime import timedelta
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serverIP = "192.168.178.51"
serverPORT = 9999
clientID = "A001"
msgLIFE = "CMD;" + clientID + ";LIFE"
sock.sendto(msgLIFE.encode(), (serverIP, serverPORT))
while True:
    timestamp_1 = datetime.datetime.now()
    state = 0
    while state != 1:
        timestamp_2 = datetime.datetime.now()
        if (timedelta.total_seconds(timestamp_2 - timestamp_1)) >= 30:
            sock.sendto(msgLIFE.encode(), (serverIP, serverPORT))
            state = 1
