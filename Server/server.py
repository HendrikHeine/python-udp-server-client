# Format: CMD;ControllerID;Command;State

import socket

server_address = '0.0.0.0'  # Listen on all
server_port = 9999  # Listen on Port 9999

controller_ID = ["A001", "A002", "A003"]
controller_IP = []

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
RECV_BUFFER = ""  # Buffer for received data
server = (server_address, server_port)
sock.bind(server)
print("Listening on " + server_address + ":" + str(server_port))

def udpSend(pack):
    RECV_BUFFER_ = ""
    #send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    pack = pack.split(";")
    ip = str(pack[0])
    port = int(pack[1])
    id_ = str(pack[2])
    cmd = str(pack[3])
    state = str(pack[4])
    msg = id_ + ";" + cmd + ";" + state
    sock.sendto(msg.encode(), (str(ip), int(port)))
    out = "OK   Message forwarded to " + "[" + str(id_) + "@" + str(ip) + ":" + str(port) + "]"
    print(str(out))
    payload = sock.recv(1024)
    RECV_BUFFER_ = str(payload)
    if RECV_BUFFER_ == "b'OK'":
        msg = "OK"
        sock.sendto(msg.encode(), (str(clientIP), int(clientPORT)) )
    else:
        msg = "FAILED"
        sock.sendto(msg.encode(), (str(clientIP), int(clientPORT)) )

while True:
    print("---")
    payload, client_address = sock.recvfrom(1024)
    clientIP = str(client_address).split("'")
    clientPORT = clientIP[2].split(" ")
    clientPORT = clientPORT[1].split(")")
    clientPORT = clientPORT[0]
    clientIP = clientIP[1]
    out = "OK     <-Received: " + str(payload)
    print(str(out))
    RECV_BUFFER = str(payload) #Saved the received Data in a Buffer
    data = RECV_BUFFER.split(";")  # Create list split by ;
    datalen = len(data)
    tempClientAddress = str(client_address)
    tempClientAddress = tempClientAddress.split(", ")
    check_CMD = data[0]
    check_CMD_ID = data[1]
    check_CMD_COMMAND = data[2]
    device_ID = str(data[1])
    device_cmd = str(data[2])

    if check_CMD == "b'CMD" and check_CMD_COMMAND == "LIFE'": #Valid Command
        
        state = 0
        count = 0
        
        lenOfController_IP = len(controller_IP)

        for item in controller_IP: #Every item in Controller IP list
            itemT = item.split(";")
            if device_ID in item:
                clientIPlist = itemT[1]
                if clientIP != clientIPlist: #If IP addr not in IP list
                    state = 1
                    newIP = str(itemT[0]) + ";" + str(clientIP)
                    del controller_IP[count]
                    print("OK    IP refreshed of item " + str(device_ID))
                    controller_IP.append(str(newIP))
                    break
                else:
                    state = 2
                    continue

            count += 1
            if (count -1)  == lenOfController_IP:
                break
        
        if lenOfController_IP == 0 or state == 0:
            print("OK    New Controller [" + str(device_ID) + "||" + str(clientIP) + "]")
            controller_IP.append(str(device_ID) + ";" + str(clientIP)) #Add the IP to the IP list with the ID

    else:
        if datalen == 4: #If Controll Command
            state = 0
            check_CMD_STATE = data[3].split("'")
            check_CMD_STATE = check_CMD_STATE[0]

            for item in controller_IP:
                if check_CMD_ID in item:
                    state = 1
                    item = item.split(";")
                    clientIP_ = item[1]
                    p = clientIP_ + ";" + "10000" + ";" + check_CMD_ID + ";" + check_CMD_COMMAND + ";" + check_CMD_STATE
                    udpSend(p)
                    break
            if state == 0:
                out = "Controller offline"
                sock.sendto(out.encode(), (str(clientIP), int(clientPORT)))
        else:
            print("ER    Unknown Command")
sock.close()